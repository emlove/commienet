Rails.application.routes.draw do
  root 'components#index'
  get '/profile/:id', to: 'components#index'
  get '/dreams', to: 'components#index'
  get '/dream/:id', to: 'components#index'
  get '/comrades', to: 'components#index'
  get '/settings', to: 'components#index'

  post '/auth/login', to: 'authentication#login'

  namespace :api do
    get '/sessionContext', to: 'session_context#get'

    get '/settings', to: 'settings#get'
    post '/settings', to: 'settings#post'

    get '/users', to: 'user#get_all'

    get '/user/:id', to: 'user#get'
    post '/user/:id', to: 'user#post'

    post '/user/:id/upload_avatar', to: 'user#upload_avatar'
    post '/user/:id/delete_avatar', to: 'user#delete_avatar'

    post '/user/:id/add_skill', to: 'user#add_skill'
    post '/user/:id/edit_skill/:skill_id', to: 'user#edit_skill'
    post '/user/:id/delete_skill/:skill_id', to: 'user#delete_skill'

    post '/user/:id/add_resource', to: 'user#add_resource'
    post '/user/:id/edit_resource/:resource_id', to: 'user#edit_resource'
    post '/user/:id/delete_resource/:resource_id', to: 'user#delete_resource'

    get '/dreams', to: 'dream#get_all'
    put '/dream', to: 'dream#put'

    get '/dream/:id', to: 'dream#get'
    post '/dream/:id', to: 'dream#post'
    delete '/dream/:id', to: 'dream#delete'

    post '/dream/:id/add_skill', to: 'dream#add_skill'
    post '/dream/:id/edit_skill/:skill_id', to: 'dream#edit_skill'
    post '/dream/:id/delete_skill/:skill_id', to: 'dream#delete_skill'

    post '/dream/:id/add_resource', to: 'dream#add_resource'
    post '/dream/:id/edit_resource/:resource_id', to: 'dream#edit_resource'
    post '/dream/:id/delete_resource/:resource_id', to: 'dream#delete_resource'
  end
end
