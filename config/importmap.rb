pin_all_from 'app/javascript/components', under: 'components'
pin_all_from 'app/javascript/lib', under: 'lib'

# Pin npm packages by running ./bin/importmap

pin 'application', preload: true
pin '@hotwired/turbo-rails', to: 'turbo.min.js', preload: true
pin 'htm', to: 'https://ga.jspm.io/npm:htm@3.1.0/dist/htm.module.js'
pin 'react', to: 'https://ga.jspm.io/npm:react@18.0.0/index.js'
pin 'react-dom', to: 'https://ga.jspm.io/npm:react-dom@18.0.0/index.js'
pin 'process',
    to:
      'https://ga.jspm.io/npm:@jspm/core@2.0.0-beta.22/nodelibs/browser/process-production.js'
pin 'scheduler', to: 'https://ga.jspm.io/npm:scheduler@0.21.0/index.js'
pin '@emotion/react',
    to:
      'https://ga.jspm.io/npm:@emotion/react@11.9.0/dist/emotion-react.browser.esm.js'
pin '@emotion/styled',
    to:
      'https://ga.jspm.io/npm:@emotion/styled@11.8.1/dist/emotion-styled.browser.esm.js'
pin '@mui/material', to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/index.js'
pin '#FormControlUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/FormControlUnstyled/index.js'
pin '#GlobalStyles',
    to: 'https://ga.jspm.io/npm:@mui/styled-engine@5.6.1/GlobalStyles/index.js'
pin '#ListboxUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/ListboxUnstyled/index.js'
pin '#MenuItemUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/MenuItemUnstyled/index.js'
pin '#MenuUnstyled',
    to: 'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/MenuUnstyled/index.js'
pin '#MultiSelectUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/MultiSelectUnstyled/index.js'
pin '#OptionGroupUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/OptionGroupUnstyled/index.js'
pin '#StyledEngineProvider',
    to:
      'https://ga.jspm.io/npm:@mui/styled-engine@5.6.1/StyledEngineProvider/index.js'
pin '#className',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/className/index.js'
pin '@babel/runtime/helpers/esm/assertThisInitialized',
    to:
      'https://ga.jspm.io/npm:@babel/runtime@7.17.9/helpers/esm/assertThisInitialized.js'
pin '@babel/runtime/helpers/esm/extends',
    to: 'https://ga.jspm.io/npm:@babel/runtime@7.17.9/helpers/esm/extends.js'
pin '@babel/runtime/helpers/esm/inheritsLoose',
    to:
      'https://ga.jspm.io/npm:@babel/runtime@7.17.9/helpers/esm/inheritsLoose.js'
pin '@babel/runtime/helpers/esm/objectWithoutPropertiesLoose',
    to:
      'https://ga.jspm.io/npm:@babel/runtime@7.17.9/helpers/esm/objectWithoutPropertiesLoose.js'
pin '@babel/runtime/helpers/extends',
    to: 'https://ga.jspm.io/npm:@babel/runtime@7.17.9/helpers/esm/extends.js'
pin '@emotion/cache',
    to:
      'https://ga.jspm.io/npm:@emotion/cache@11.7.1/dist/emotion-cache.browser.esm.js'
pin '@emotion/hash',
    to: 'https://ga.jspm.io/npm:@emotion/hash@0.8.0/dist/hash.browser.esm.js'
pin '@emotion/is-prop-valid',
    to:
      'https://ga.jspm.io/npm:@emotion/is-prop-valid@1.1.2/dist/emotion-is-prop-valid.browser.esm.js'
pin '@emotion/memoize',
    to:
      'https://ga.jspm.io/npm:@emotion/memoize@0.7.5/dist/emotion-memoize.browser.esm.js'
pin '@emotion/serialize',
    to:
      'https://ga.jspm.io/npm:@emotion/serialize@1.0.3/dist/emotion-serialize.browser.esm.js'
pin '@emotion/sheet',
    to:
      'https://ga.jspm.io/npm:@emotion/sheet@1.1.0/dist/emotion-sheet.browser.esm.js'
pin '@emotion/unitless',
    to:
      'https://ga.jspm.io/npm:@emotion/unitless@0.7.5/dist/unitless.browser.esm.js'
pin '@emotion/utils',
    to:
      'https://ga.jspm.io/npm:@emotion/utils@1.1.0/dist/emotion-utils.browser.esm.js'
pin '@emotion/weak-memoize',
    to:
      'https://ga.jspm.io/npm:@emotion/weak-memoize@0.2.5/dist/weak-memoize.browser.esm.js'
pin '@mui/base', to: 'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/index.js'
pin '@mui/base/AutocompleteUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/AutocompleteUnstyled/index.js'
pin '@mui/base/BadgeUnstyled',
    to: 'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/BadgeUnstyled/index.js'
pin '@mui/base/ButtonUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/ButtonUnstyled/index.js'
pin '@mui/base/ClickAwayListener',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/ClickAwayListener/index.js'
pin '@mui/base/InputUnstyled',
    to: 'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/InputUnstyled/index.js'
pin '@mui/base/ModalUnstyled',
    to: 'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/ModalUnstyled/index.js'
pin '@mui/base/NoSsr',
    to: 'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/NoSsr/index.js'
pin '@mui/base/OptionUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/OptionUnstyled/index.js'
pin '@mui/base/PopperUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/PopperUnstyled/index.js'
pin '@mui/base/Portal',
    to: 'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/Portal/index.js'
pin '@mui/base/SelectUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/SelectUnstyled/index.js'
pin '@mui/base/SliderUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/SliderUnstyled/index.js'
pin '@mui/base/SwitchUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/SwitchUnstyled/index.js'
pin '@mui/base/TabPanelUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/TabPanelUnstyled/index.js'
pin '@mui/base/TabUnstyled',
    to: 'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/TabUnstyled/index.js'
pin '@mui/base/TabsListUnstyled',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/TabsListUnstyled/index.js'
pin '@mui/base/TabsUnstyled',
    to: 'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/TabsUnstyled/index.js'
pin '@mui/base/TextareaAutosize',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/TextareaAutosize/index.js'
pin '@mui/base/TrapFocus',
    to: 'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/TrapFocus/index.js'
pin '@mui/base/className',
    to: 'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/className/index.js'
pin '@mui/base/composeClasses',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/composeClasses/index.js'
pin '@mui/base/generateUtilityClass',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/generateUtilityClass/index.js'
pin '@mui/base/generateUtilityClasses',
    to:
      'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/generateUtilityClasses/index.js'
pin '@mui/base/utils',
    to: 'https://ga.jspm.io/npm:@mui/base@5.0.0-alpha.76/utils/index.js'
pin '@mui/material/Accordion',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Accordion/index.js'
pin '@mui/material/AccordionActions',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/AccordionActions/index.js'
pin '@mui/material/AccordionDetails',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/AccordionDetails/index.js'
pin '@mui/material/AccordionSummary',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/AccordionSummary/index.js'
pin '@mui/material/Alert',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Alert/index.js'
pin '@mui/material/AlertTitle',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/AlertTitle/index.js'
pin '@mui/material/AppBar',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/AppBar/index.js'
pin '@mui/material/Autocomplete',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Autocomplete/index.js'
pin '@mui/material/Avatar',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Avatar/index.js'
pin '@mui/material/AvatarGroup',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/AvatarGroup/index.js'
pin '@mui/material/Backdrop',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Backdrop/index.js'
pin '@mui/material/Badge',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Badge/index.js'
pin '@mui/material/BottomNavigation',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/BottomNavigation/index.js'
pin '@mui/material/BottomNavigationAction',
    to:
      'https://ga.jspm.io/npm:@mui/material@5.6.1/BottomNavigationAction/index.js'
pin '@mui/material/Box',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Box/index.js'
pin '@mui/material/Breadcrumbs',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Breadcrumbs/index.js'
pin '@mui/material/Button',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Button/index.js'
pin '@mui/material/ButtonBase',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ButtonBase/index.js'
pin '@mui/material/ButtonGroup',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ButtonGroup/index.js'
pin '@mui/material/Card',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Card/index.js'
pin '@mui/material/CardActionArea',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/CardActionArea/index.js'
pin '@mui/material/CardActions',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/CardActions/index.js'
pin '@mui/material/CardContent',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/CardContent/index.js'
pin '@mui/material/CardHeader',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/CardHeader/index.js'
pin '@mui/material/CardMedia',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/CardMedia/index.js'
pin '@mui/material/Checkbox',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Checkbox/index.js'
pin '@mui/material/Chip',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Chip/index.js'
pin '@mui/material/CircularProgress',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/CircularProgress/index.js'
pin '@mui/material/ClickAwayListener',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ClickAwayListener/index.js'
pin '@mui/material/Collapse',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Collapse/index.js'
pin '@mui/material/Container',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Container/index.js'
pin '@mui/material/CssBaseline',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/CssBaseline/index.js'
pin '@mui/material/Dialog',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Dialog/index.js'
pin '@mui/material/DialogActions',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/DialogActions/index.js'
pin '@mui/material/DialogContent',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/DialogContent/index.js'
pin '@mui/material/DialogContentText',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/DialogContentText/index.js'
pin '@mui/material/DialogTitle',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/DialogTitle/index.js'
pin '@mui/material/Divider',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Divider/index.js'
pin '@mui/material/Drawer',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Drawer/index.js'
pin '@mui/material/Fab',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Fab/index.js'
pin '@mui/material/Fade',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Fade/index.js'
pin '@mui/material/FilledInput',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/FilledInput/index.js'
pin '@mui/material/FormControl',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/FormControl/index.js'
pin '@mui/material/FormControlLabel',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/FormControlLabel/index.js'
pin '@mui/material/FormGroup',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/FormGroup/index.js'
pin '@mui/material/FormHelperText',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/FormHelperText/index.js'
pin '@mui/material/FormLabel',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/FormLabel/index.js'
pin '@mui/material/GlobalStyles',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/GlobalStyles/index.js'
pin '@mui/material/Grid',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Grid/index.js'
pin '@mui/material/Grow',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Grow/index.js'
pin '@mui/material/Hidden',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Hidden/index.js'
pin '@mui/material/Icon',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Icon/index.js'
pin '@mui/material/IconButton',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/IconButton/index.js'
pin '@mui/material/ImageList',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ImageList/index.js'
pin '@mui/material/ImageListItem',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ImageListItem/index.js'
pin '@mui/material/ImageListItemBar',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ImageListItemBar/index.js'
pin '@mui/material/Input',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Input/index.js'
pin '@mui/material/InputAdornment',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/InputAdornment/index.js'
pin '@mui/material/InputBase',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/InputBase/index.js'
pin '@mui/material/InputLabel',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/InputLabel/index.js'
pin '@mui/material/LinearProgress',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/LinearProgress/index.js'
pin '@mui/material/Link',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Link/index.js'
pin '@mui/material/List',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/List/index.js'
pin '@mui/material/ListItem',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ListItem/index.js'
pin '@mui/material/ListItemAvatar',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ListItemAvatar/index.js'
pin '@mui/material/ListItemButton',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ListItemButton/index.js'
pin '@mui/material/ListItemIcon',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ListItemIcon/index.js'
pin '@mui/material/ListItemSecondaryAction',
    to:
      'https://ga.jspm.io/npm:@mui/material@5.6.1/ListItemSecondaryAction/index.js'
pin '@mui/material/ListItemText',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ListItemText/index.js'
pin '@mui/material/ListSubheader',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ListSubheader/index.js'
pin '@mui/material/Menu',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Menu/index.js'
pin '@mui/material/MenuItem',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/MenuItem/index.js'
pin '@mui/material/MenuList',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/MenuList/index.js'
pin '@mui/material/MobileStepper',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/MobileStepper/index.js'
pin '@mui/material/Modal',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Modal/index.js'
pin '@mui/material/NativeSelect',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/NativeSelect/index.js'
pin '@mui/material/NoSsr',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/NoSsr/index.js'
pin '@mui/material/OutlinedInput',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/OutlinedInput/index.js'
pin '@mui/material/Pagination',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Pagination/index.js'
pin '@mui/material/PaginationItem',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/PaginationItem/index.js'
pin '@mui/material/Paper',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Paper/index.js'
pin '@mui/material/Popover',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Popover/index.js'
pin '@mui/material/Popper',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Popper/index.js'
pin '@mui/material/Portal',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Portal/index.js'
pin '@mui/material/Radio',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Radio/index.js'
pin '@mui/material/RadioGroup',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/RadioGroup/index.js'
pin '@mui/material/Rating',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Rating/index.js'
pin '@mui/material/ScopedCssBaseline',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ScopedCssBaseline/index.js'
pin '@mui/material/Select',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Select/index.js'
pin '@mui/material/Skeleton',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Skeleton/index.js'
pin '@mui/material/Slide',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Slide/index.js'
pin '@mui/material/Slider',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Slider/index.js'
pin '@mui/material/Snackbar',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Snackbar/index.js'
pin '@mui/material/SnackbarContent',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/SnackbarContent/index.js'
pin '@mui/material/SpeedDial',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/SpeedDial/index.js'
pin '@mui/material/SpeedDialAction',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/SpeedDialAction/index.js'
pin '@mui/material/SpeedDialIcon',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/SpeedDialIcon/index.js'
pin '@mui/material/Stack',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Stack/index.js'
pin '@mui/material/Step',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Step/index.js'
pin '@mui/material/StepButton',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/StepButton/index.js'
pin '@mui/material/StepConnector',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/StepConnector/index.js'
pin '@mui/material/StepContent',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/StepContent/index.js'
pin '@mui/material/StepIcon',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/StepIcon/index.js'
pin '@mui/material/StepLabel',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/StepLabel/index.js'
pin '@mui/material/Stepper',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Stepper/index.js'
pin '@mui/material/SvgIcon',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/SvgIcon/index.js'
pin '@mui/material/SwipeableDrawer',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/SwipeableDrawer/index.js'
pin '@mui/material/Switch',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Switch/index.js'
pin '@mui/material/Tab',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Tab/index.js'
pin '@mui/material/TabScrollButton',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/TabScrollButton/index.js'
pin '@mui/material/Table',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Table/index.js'
pin '@mui/material/TableBody',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/TableBody/index.js'
pin '@mui/material/TableCell',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/TableCell/index.js'
pin '@mui/material/TableContainer',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/TableContainer/index.js'
pin '@mui/material/TableFooter',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/TableFooter/index.js'
pin '@mui/material/TableHead',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/TableHead/index.js'
pin '@mui/material/TablePagination',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/TablePagination/index.js'
pin '@mui/material/TableRow',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/TableRow/index.js'
pin '@mui/material/TableSortLabel',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/TableSortLabel/index.js'
pin '@mui/material/Tabs',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Tabs/index.js'
pin '@mui/material/TextField',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/TextField/index.js'
pin '@mui/material/TextareaAutosize',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/TextareaAutosize/index.js'
pin '@mui/material/ToggleButton',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ToggleButton/index.js'
pin '@mui/material/ToggleButtonGroup',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/ToggleButtonGroup/index.js'
pin '@mui/material/Toolbar',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Toolbar/index.js'
pin '@mui/material/Tooltip',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Tooltip/index.js'
pin '@mui/material/Typography',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Typography/index.js'
pin '@mui/material/Zoom',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/Zoom/index.js'
pin '@mui/material/colors',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/colors/index.js'
pin '@mui/material/darkScrollbar',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/darkScrollbar/index.js'
pin '@mui/material/styles',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/styles/index.js'
pin '@mui/material/useAutocomplete',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/useAutocomplete/index.js'
pin '@mui/material/useMediaQuery',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/useMediaQuery/index.js'
pin '@mui/material/usePagination',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/usePagination/index.js'
pin '@mui/material/useScrollTrigger',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/useScrollTrigger/index.js'
pin '@mui/material/utils',
    to: 'https://ga.jspm.io/npm:@mui/material@5.6.1/utils/index.js'
pin '@mui/private-theming',
    to: 'https://ga.jspm.io/npm:@mui/private-theming@5.6.1/index.js'
pin '@mui/private-theming/ThemeProvider',
    to:
      'https://ga.jspm.io/npm:@mui/private-theming@5.6.1/ThemeProvider/index.js'
pin '@mui/private-theming/useTheme',
    to: 'https://ga.jspm.io/npm:@mui/private-theming@5.6.1/useTheme/index.js'
pin '@mui/styled-engine',
    to: 'https://ga.jspm.io/npm:@mui/styled-engine@5.6.1/index.js'
pin '@mui/system', to: 'https://ga.jspm.io/npm:@mui/system@5.6.1/esm/index.js'
pin '@mui/utils', to: 'https://ga.jspm.io/npm:@mui/utils@5.6.1/esm/index.js'
pin '@popperjs/core',
    to: 'https://ga.jspm.io/npm:@popperjs/core@2.11.5/lib/index.js'
pin 'clsx', to: 'https://ga.jspm.io/npm:clsx@1.1.1/dist/clsx.m.js'
pin 'dom-helpers/addClass',
    to: 'https://ga.jspm.io/npm:dom-helpers@5.2.0/esm/addClass.js'
pin 'dom-helpers/removeClass',
    to: 'https://ga.jspm.io/npm:dom-helpers@5.2.0/esm/removeClass.js'
pin 'hoist-non-react-statics',
    to:
      'https://ga.jspm.io/npm:hoist-non-react-statics@3.3.2/dist/hoist-non-react-statics.cjs.js'
pin 'prop-types', to: 'https://ga.jspm.io/npm:prop-types@15.8.1/index.js'
pin 'react-is', to: 'https://ga.jspm.io/npm:react-is@17.0.2/index.js'
pin 'react-transition-group',
    to: 'https://ga.jspm.io/npm:react-transition-group@4.4.2/esm/index.js'
pin 'react/jsx-runtime',
    to: 'https://ga.jspm.io/npm:react@18.0.0/jsx-runtime.js'
pin 'stylis', to: 'https://ga.jspm.io/npm:stylis@4.0.13/index.js'
pin '@mui/icons-material',
    to: 'https://ga.jspm.io/npm:@mui/icons-material@5.6.1/esm/index.js'
pin 'react-router-dom',
    to: 'https://ga.jspm.io/npm:react-router-dom@6.3.0/index.js'
pin 'history', to: 'https://ga.jspm.io/npm:history@5.2.0/index.js'
pin 'react-router', to: 'https://ga.jspm.io/npm:react-router@6.3.0/index.js'
