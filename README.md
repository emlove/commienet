# Commienet

An open source app to build communities of anti-capitalist mutual aid.

## Developing

Install [rbenv](https://github.com/rbenv/rbenv#installation)
Install [yarn](https://classic.yarnpkg.com/lang/en/docs/install/)

```bash
cd commienet
yarn install
rbenv install
gem install bundler
bundle
rails c
> User.create(name: "Saturday Valentine", email: "saturday@example.com", password: "foo") # Create a user for yourself
rails s
```

[Open the app locally](http://localhost:1312/)
