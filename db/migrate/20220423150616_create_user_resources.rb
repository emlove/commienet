class CreateUserResources < ActiveRecord::Migration[7.0]
  def change
    create_table :user_resources do |t|
      t.integer :user_id, null: false
      t.integer :type, null: false
      t.string :description, null: false, default: ''

      t.timestamps
    end

    add_index :user_resources, %i[user_id type], unique: true
  end
end
