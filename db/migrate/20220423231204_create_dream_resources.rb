class CreateDreamResources < ActiveRecord::Migration[7.0]
  def change
    create_table :dream_resources do |t|
      t.references :dream, null: false, foreign_key: true
      t.integer :type
      t.string :description

      t.timestamps
    end
  end
end
