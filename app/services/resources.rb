module Resources
  TYPE_ENUM = {
    money: 1,
    vehicle: 2,
    housing: 3,
    workspace: 4,
    materials: 5,
    food: 6,
    clothing: 7,
    furniture: 8,
    tools: 9,
    entertainment: 10
  }.with_indifferent_access

  TYPE_NAMES = {
    money: 'Money',
    vehicle: 'Vehicle',
    housing: 'Housing',
    workspace: 'Workspace',
    materials: 'Materials',
    food: 'Food',
    clothing: 'Clothing',
    furniture: 'Furniture',
    tools: 'Tools',
    entertainment: 'Entertainment'
  }.with_indifferent_access
end
