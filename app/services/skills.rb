module Skills
  TYPE_ENUM = {
    animals: 1,
    plants: 2,
    languages: 3,
    home_improvement: 4,
    food_prep: 5,
    art: 6,
    cleaning: 7,
    physical_labor: 8,
    emotional_labor: 9,
    community_organizing: 10,
    sewing: 11
  }

  TYPE_NAMES = {
    animals: 'Animals',
    plants: 'Plants',
    languages: 'Languages',
    home_improvement: 'Home Improvement',
    food_prep: 'Food Prep',
    art: 'Art',
    cleaning: 'Cleaning',
    physical_labor: 'Physical Labor',
    emotional_labor: 'Emotional Labor',
    community_organizing: 'Community Organizing',
    sewing: 'Sewing'
  }.with_indifferent_access
end
