import { useState, useEffect } from "react";
import html from "lib/htmCreateElement";
import { Navigate, Routes, Route, useRoutes } from "react-router-dom";

import Stack from "@mui/material/Stack";
import Container from "@mui/material/Container";

import App from "components/App";
import Login from "components/Login";
import Profile from "components/Profile";
import Dream from "components/Dream";
import Dreams from "components/Dreams";
import Comrades from "components/Comrades";
import Settings from "components/Settings";
import TitleBar from "components/TitleBar";
import NavigationDrawer from "components/NavigationDrawer";
import NavigationMenu from "components/NavigationMenu";

import Loading from "components/Shared/Loading";

import { AuthContext } from "lib/api";

export default ({ children }) => {
  const [authContext, setAuthContext] = useState(() =>
    JSON.parse(localStorage.getItem("authContext"))
  );

  useEffect(() => {
    localStorage.setItem("authContext", JSON.stringify(authContext));
  }, [authContext]);

  if (authContext === null) {
    return html`<${Login} setAuthContext=${setAuthContext} />`;
  }

  return html`
    <${AuthContext.Provider}
      value=${{
        ...authContext,
        logout: () => setAuthContext(null),
      }}
      >${children}<//
    >
  `;
};
