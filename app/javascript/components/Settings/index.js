import html from "lib/htmCreateElement";
import { useState, useEffect } from "react";

import { useApi } from "lib/api";

import Alert from "@mui/material/Alert";
import Container from "@mui/material/Container";

import SettingsForm from "components/Settings/Form";
import Loading from "components/Shared/Loading";

export default ({ setPageTitle }) => {
  const { apiGet, apiPost } = useApi();
  const [errorMessage, setErrorMessage] = useState(null);
  const [settings, setSettings] = useState(null);

  useEffect(() => {
    setPageTitle("Settings");
    setErrorMessage(null);

    apiGet("settings")
      .then(setSettings)
      .catch((e) => setErrorMessage(e.message));
  }, []);

  const handleSettingsSaved = async (settings) => {
    setSettings(await apiPost("settings", settings));
  };

  if (errorMessage) {
    return html`<${Alert} severity="error">${errorMessage}<//>`;
  }

  if (settings === null) {
    return html`<${Loading} />`;
  }

  return html`
    <${Container}
      maxwidth="xs"
      sx=${{ display: "flex", justifyContent: "center" }}
    >
      <${SettingsForm} settings=${settings} onSaved=${handleSettingsSaved} />
    <//>
  `;
};
