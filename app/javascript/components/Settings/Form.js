import html from "lib/htmCreateElement";
import { useState } from "react";

import Button from "@mui/material/Button";
import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";

import SubmitButton from "components/Shared/SubmitButton";

export default ({ settings, onSaved }) => {
  const [email, setEmail] = useState(settings.email);
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState(null);

  const emailValid = email !== "";
  const confirmPasswordValid =
    confirmPassword === null ? null : password === confirmPassword;

  const formReady = emailValid && (!password || confirmPasswordValid);

  const formData = {
    email,
    ...(confirmPasswordValid ? { password } : {}),
  };

  const onFieldKeyPress = (e) => {
    if (e.which === 13 && formReady) {
      e.preventDefault();
      return onSaved(formData);
    }
  };

  return html`
    <${Stack} spacing=${2} sx=${{ marginTop: 2, width: "100%" }}>
      <${TextField}
        id="email"
        label="Email address"
        type="email"
        required
        value=${email}
        onChange=${(e) => setEmail(e.target.value)}
        onKeyPress=${onFieldKeyPress}
        error=${emailValid === false}
        helperText=${emailValid === false ? "Required" : undefined}
      />
      <${TextField}
        id="password"
        label="Password"
        type="password"
        value=${password}
        onChange=${(e) => setPassword(e.target.value)}
        onKeyPress=${onFieldKeyPress}
      />
      <${TextField}
        id="confirmPassword"
        label="Confirm password"
        type="password"
        value=${confirmPassword || ""}
        onChange=${(e) => setConfirmPassword(e.target.value)}
        onKeyPress=${onFieldKeyPress}
        error=${confirmPasswordValid === false}
        helperText=${confirmPasswordValid === false
          ? "Passwords do not match"
          : undefined}
      />
      <${Divider} />
      <${SubmitButton}
        id="save"
        type="submit"
        variant="contained"
        disabled=${!formReady}
        onClick=${() => onSaved(formData)}
        successText="Saved!"
        >Save<//
      >
      <${Typography} variant="caption" sx=${{ flexGrow: 1 }}
        >Commienet -${" "}
        <${Link} href="https://gitlab.com/emlove/commienet">Source<//><//
      >
    <//>
  `;
};
