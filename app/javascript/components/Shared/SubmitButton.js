import html from "lib/htmCreateElement";
import { useState } from "react";

import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";

export default ({
  onClick,
  children,
  successText,
  component = Button,
  ...props
}) => {
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [isComplete, setIsComplete] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  const handleClick = async () => {
    setIsComplete(false);
    setErrorMessage(null);
    setIsSubmitting(true);

    try {
      await onClick();
      setIsComplete(true);
      setTimeout(() => setIsComplete(false), 5000);
    } catch (e) {
      setErrorMessage(e.message);
    } finally {
      setIsSubmitting(false);
    }
  };

  return html`
    ${successText && isComplete
      ? html` <${Typography}
          variant="button"
          color="success.main"
          sx=${{ marginRight: 2 }}
          >${successText}<//
        >`
      : null}
    ${errorMessage
      ? html` <${Typography}
          variant="button"
          color="error.main"
          sx=${{ marginRight: 2 }}
          >${errorMessage}<//
        >`
      : null}
    <${component}
      onClick=${handleClick}
      disabled=${props.disabled || isSubmitting}
      ...${props}
      >${children}<//
    >
  `;
};
