import html from "lib/htmCreateElement";

import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";

export default () => {
  return html`
    <${Box}
      sx=${{
        display: "flex",
        justifyContent: "center",
        width: "100%",
      }}
    >
      <${CircularProgress} />
    <//>
  `;
};
