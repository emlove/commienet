import html from "lib/htmCreateElement";
import { useContext } from "react";

import Divider from "@mui/material/Divider";
import { Link } from "react-router-dom";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";

import { Star as StarIcon } from "@mui/icons-material";
import { People as PeopleIcon } from "@mui/icons-material";
import { Settings as SettingsIcon } from "@mui/icons-material";

export default () => {
  return html`
    <${List}>
      <${ListItem}>
        <${ListItemButton} component=${Link} to="/dreams">
          <${ListItemIcon}>
            <${StarIcon} color="secondary" />
          <//>
          <${ListItemText}> Dreams <//>
        <//>
      <//>
      <${ListItem}>
        <${ListItemButton} component=${Link} to="/comrades">
          <${ListItemIcon}>
            <${PeopleIcon} color="secondary" />
          <//>
          <${ListItemText}> Comrades <//>
        <//>
      <//>
      <${ListItem}>
        <${ListItemButton} component=${Link} to="/settings">
          <${ListItemIcon}>
            <${SettingsIcon} color="secondary" />
          <//>
          <${ListItemText}> Settings <//>
        <//>
      <//>
    <//>
  `;
};
