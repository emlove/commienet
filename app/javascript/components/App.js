import { useState, useEffect } from "react";
import html from "lib/htmCreateElement";
import { Navigate, Routes, Route, useRoutes } from "react-router-dom";

import Alert from "@mui/material/Alert";
import Stack from "@mui/material/Stack";
import Container from "@mui/material/Container";

import Login from "components/Login";
import Profile from "components/Profile";
import Dream from "components/Dream";
import Dreams from "components/Dreams";
import Comrades from "components/Comrades";
import Settings from "components/Settings";
import TitleBar from "components/TitleBar";
import NavigationDrawer from "components/NavigationDrawer";
import NavigationMenu from "components/NavigationMenu";

import Loading from "components/Shared/Loading";

import { useApi } from "lib/api";
import { SessionContext } from "lib/sessionContext";

export default () => {
  const { apiGet } = useApi();
  const [navigationDrawerOpen, setNavigationDrawerOpen] = useState(false);

  const [sessionContext, setSessionContext] = useState(null);
  const [pageTitle, setPageTitle] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);

  useEffect(() => {
    apiGet("sessionContext")
      .then(setSessionContext)
      .catch((e) => setErrorMessage(e.message));
  }, []);

  if (errorMessage) {
    return html` <${Stack}
      spacing=${2}
      alignItems="center"
      justifyContent="center"
      sx=${{ minHeight: "100vh" }}
    >
      <${Alert} severity="error">${errorMessage}<//><//
    >`;
  }

  if (sessionContext === null) {
    return html` <${Stack}
      spacing=${2}
      alignItems="center"
      justifyContent="center"
      sx=${{ minHeight: "100vh" }}
    >
      ><${Loading}
    /><//>`;
  }

  return html`
    <${SessionContext.Provider} value=${sessionContext}>
      <${NavigationDrawer}
        open=${navigationDrawerOpen}
        onClose=${() => setNavigationDrawerOpen(false)}
      >
        <${NavigationMenu} />
      <//>
      <${Stack} sx=${{ minHeight: "100vh" }}>
        <${TitleBar}
          onMenuButtonClicked=${() => setNavigationDrawerOpen(true)}
          title=${pageTitle}
        />
        <${Container} maxwidth="sm" sx=${{ flex: 1 }}>
          <${Routes}>
            <${Route}
              path="/"
              element=${html`<${Navigate}
                to=${`/profile/${sessionContext.currentUser.id}`}
              />`}
            />
            <${Route}
              path="/profile/:id"
              element=${html`<${Profile} setPageTitle=${setPageTitle} />`}
            />
            <${Route}
              path="/dreams"
              element=${html`<${Dreams} setPageTitle=${setPageTitle} />`}
            />
            <${Route}
              path="/dream/:id"
              element=${html`<${Dream} setPageTitle=${setPageTitle} />`}
            />
            <${Route}
              path="/comrades"
              element=${html`<${Comrades} setPageTitle=${setPageTitle} />`}
            />
            <${Route}
              path="/settings"
              element=${html`<${Settings} setPageTitle=${setPageTitle} />`}
            />
          <//>
        <//>
      <//>
    <//>
  `;
};
