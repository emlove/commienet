import html from "lib/htmCreateElement";
import { useState } from "react";

import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import { Delete as DeleteIcon } from "@mui/icons-material";
import IconButton from "@mui/material/IconButton";
import TextField from "@mui/material/TextField";

import SubmitButton from "components/Shared/SubmitButton";

import { useApi } from "lib/api";

export default ({ resource, user, setUser }) => {
  const { apiPost } = useApi();

  const [description, setDescription] = useState(resource.description);

  const handleSave = async () => {
    setUser(
      await apiPost(`user/${user.id}/edit_resource/${resource.id}`, {
        description: description,
      })
    );
  };

  const handleDelete = async () => {
    setUser(await apiPost(`user/${user.id}/delete_resource/${resource.id}`));
  };

  return html`
    <${Card}>
      <${CardHeader}
        title=${resource.name}
        action=${html` <${SubmitButton}
          component=${IconButton}
          onClick=${handleDelete}
        >
          <${DeleteIcon} />
        <//>`}
      />
      <${CardContent}>
        <${TextField}
          id="description"
          label="Description"
          value=${description}
          multiline
          minRows="{4}"
          onChange=${(e) => setDescription(e.target.value)}
        />
      <//>
      <${CardActions} sx=${{ justifyContent: "end" }}>
        <${SubmitButton} id="save" onClick=${handleSave} successText="Saved!"
          >Save<//
        >
      <//>
    <//>
  `;
};
