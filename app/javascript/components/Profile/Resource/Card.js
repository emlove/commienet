import html from "lib/htmCreateElement";

import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

export default ({ resource }) => {
  return html`
    <${Card}>
      <${CardHeader} title=${resource.name} />
      <${CardContent}>
        <${Typography} paragraph sx=${{ whiteSpace: "pre-line" }}>
          ${resource.description}
        <//>
      <//>
    <//>
  `;
};
