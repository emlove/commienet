import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import html from "lib/htmCreateElement";

import { useApi } from "lib/api";
import { useCurrentUser } from "lib/sessionContext";

import Alert from "@mui/material/Alert";
import ProfileView from "components/Profile/View";
import ProfileEdit from "components/Profile/Edit";
import Loading from "components/Shared/Loading";

export default ({ setPageTitle }) => {
  const id = parseInt(useParams()["id"]);
  const { apiGet } = useApi();
  const [errorMessage, setErrorMessage] = useState(null);
  const [user, setUser] = useState(null);

  useEffect(() => {
    setErrorMessage(null);

    apiGet(`user/${id}`)
      .then((user) => {
        setUser(user);
        setPageTitle(user.name);
      })
      .catch((e) => setErrorMessage(e.message));
  }, [id]);

  if (errorMessage) {
    return html`<${Alert} severity="error">${errorMessage}<//>`;
  }

  if (user === null) {
    return html`<${Loading} />`;
  }

  if (useCurrentUser().id === id) {
    return html`<${ProfileEdit}
      key=${user.id}
      user=${user}
      setUser=${setUser}
    />`;
  }

  return html`<${ProfileView} key=${user.id} user=${user} />`;
};
