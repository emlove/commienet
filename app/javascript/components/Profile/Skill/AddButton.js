import { useState } from "react";
import html from "lib/htmCreateElement";

import Alert from "@mui/material/Alert";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";

import SkillAddForm from "components/Profile/Skill/AddForm";

import { useApi } from "lib/api";

export default ({ user, setUser }) => {
  const { apiPost } = useApi();
  const [dialogOpen, setDialogOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  const onSkillAdded = async (skillType) => {
    setErrorMessage(null);
    try {
      setUser(
        await apiPost(`user/${user.id}/add_skill`, {
          skill_type: skillType.type,
        })
      );
      setDialogOpen(false);
    } catch (e) {
      setErrorMessage(e.message);
    }
  };

  return html`
    <${Button} id="add" variant="outlined" onClick=${() => setDialogOpen(true)}>
      Add a skill
    <//>
    <${Dialog} open=${dialogOpen} onClose=${() => setDialogOpen(false)}>
      <${DialogTitle}>Add a skill<//>
      <${DialogContent}>
        ${errorMessage
          ? html`<${Alert} severity="error" sx=${{ marginBottom: 2 }}
              >${errorMessage}<//
            >`
          : null}
        <${SkillAddForm} onSkillAdded=${onSkillAdded} />
      <//>
    <//>
  `;
};
