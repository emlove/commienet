import { createRef, useState } from "react";
import { useParams } from "react-router-dom";
import html from "lib/htmCreateElement";

import Avatar from "@mui/material/Avatar";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import CardHeader from "@mui/material/CardHeader";
import TextField from "@mui/material/TextField";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import Button from "@mui/material/Button";
import Badge from "@mui/material/Badge";
import Stack from "@mui/material/Stack";

import { Delete as DeleteIcon } from "@mui/icons-material";

import SkillAddButton from "components/Profile/Skill/AddButton";
import SkillEditCard from "components/Profile/Skill/EditCard";
import ResourceAddButton from "components/Profile/Resource/AddButton";
import ResourceEditCard from "components/Profile/Resource/EditCard";
import SubmitButton from "components/Shared/SubmitButton";

import { useApi } from "lib/api";

export default ({ user, setUser }) => {
  const { apiPost, apiPostFiles } = useApi();
  const [name, setName] = useState(user.name);
  const [bio, setBio] = useState(user.bio);
  const avatarFileRef = createRef(null);

  const handleSaveProfile = async () => {
    setUser(
      await apiPost(`user/${user.id}`, {
        name,
        bio,
      })
    );
  };

  const handleAvatarFileSelected = async (avatar) => {
    setUser(await apiPostFiles(`user/${user.id}/upload_avatar`, { avatar }));
  };

  const handleAvatarDeleted = async () => {
    setUser(await apiPost(`user/${user.id}/delete_avatar`));
  };

  const renderAvatar = () => html`
    <${Avatar}
      sx=${{ width: 72, height: 72 }}
      src=${user.avatarUrl || undefined}
    />
  `;

  const renderAvatarHeader = () => html`
    <${Button} component="label">
      <input
        id="avatar"
        type="file"
        accept="image/*"
        hidden
        ref=${avatarFileRef}
        onChange=${async (e) => handleAvatarFileSelected(e.target?.files?.[0])}
      />
      ${user.avatarUrl
        ? html`
            <${Badge}
              anchorOrigin=${{ vertical: "top", horizontal: "right" }}
              badgeContent=${html`
                <${IconButton} id="avatarDelete" onClick=${handleAvatarDeleted}>
                  <${DeleteIcon} />
                <//>
              `}
            >
              ${renderAvatar()}
            <//>
          `
        : renderAvatar()}
    <//>
  `;

  return html`
    <${Stack} spacing=${2} alignItems="center" justifyContent="center">
      <${CardHeader}
        avatar=${renderAvatarHeader()}
        title=${html`<${TextField}
          id="name"
          label="Name"
          value=${name}
          onChange=${(e) => setName(e.target.value)}
          fullWidth
        /> `}
      />
      <${TextField}
        id="bio"
        label="Bio"
        multiline
        fullWidth
        value=${bio}
        onChange=${(e) => setBio(e.target.value)}
        minRows=${4}
      />
      <${Stack} direction="row" justifyContent="end" alignItems="center">
        <${SubmitButton} onClick=${handleSaveProfile} successText="Saved!"
          >Save<//
        >
      <//>
      <${Divider} />
      <${Grid}
        container
        spacing=${2}
        alignItems="center"
        justifyContent="center"
      >
        ${user.skills.map(
          (skill) => html`
            <${Grid} item>
              <${SkillEditCard}
                user=${user}
                setUser=${setUser}
                skill=${skill}
              />
            <//>
          `
        )}
        <${Grid} item>
          <${SkillAddButton} user=${user} setUser=${setUser} />
        <//>
      <//>
      <${Divider} />
      <${Grid}
        container
        spacing=${2}
        alignItems="center"
        justifyContent="center"
      >
        ${user.resources.map(
          (resource) => html`
            <${Grid} item>
              <${ResourceEditCard}
                user=${user}
                setUser=${setUser}
                resource=${resource}
              />
            <//>
          `
        )}
        <${Grid} item>
          <${ResourceAddButton} user=${user} setUser=${setUser} />
        <//>
      <//>
    <//>
  `;
};
