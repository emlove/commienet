import html from "lib/htmCreateElement";

import Avatar from "@mui/material/Avatar";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import CardHeader from "@mui/material/CardHeader";
import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";

import SkillCard from "components/Profile/Skill/Card";
import ResourceCard from "components/Profile/Resource/Card";

export default ({ user }) => {
  return html`
    <${Stack} spacing=${2} alignItems="center" justifyContent="center">
      <${CardHeader}
        avatar=${html`<${Avatar}
          src=${user.avatarUrl || undefined}
          sx=${{ width: 72, height: 72 }}
        />`}
        title=${html`<${Typography} variant="h3">${user.name}<//>`}
      />
      <${Typography} paragraph sx=${{ whiteSpace: "pre-line" }}>${user.bio}<//>
      <${Divider} />
      <${Grid}
        container
        spacing=${2}
        alignItems="center"
        justifyContent="center"
      >
        ${user.skills.map(
          (skill) => html`
            <${Grid} item>
              <${SkillCard} skill=${skill} />
            <//>
          `
        )}
      <//>
      <${Divider} />
      <${Grid}
        container
        spacing=${2}
        alignItems="center"
        justifyContent="center"
      >
        ${user.resources.map(
          (resource) => html`
            <${Grid} item>
              <${ResourceCard} resource=${resource} />
            <//>
          `
        )}
      <//>
    <//>
  `;
};
