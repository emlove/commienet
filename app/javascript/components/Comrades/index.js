import html from "lib/htmCreateElement";
import { useState, useEffect } from "react";

import Alert from "@mui/material/Alert";
import Grid from "@mui/material/Grid";
import Stack from "@mui/material/Stack";

import { useApi } from "lib/api";

import UserCard from "components/Comrades/Card";
import Loading from "components/Shared/Loading";

export default ({ setPageTitle }) => {
  const { apiGet } = useApi();
  const [errorMessage, setErrorMessage] = useState(null);
  const [users, setUsers] = useState(null);

  useEffect(() => {
    setPageTitle("Comrades");
    setErrorMessage(null);

    apiGet("users")
      .then(setUsers)
      .catch((e) => setErrorMessage(e.message));
  }, []);

  if (errorMessage) {
    return html`<${Alert} severity="error">${errorMessage}<//>`;
  }

  if (users === null) {
    return html`<${Loading} />`;
  }

  if (users.length === 0) {
    return html`
      <${Stack}
        spacing=${2}
        direction="column"
        alignItems="center"
        justifyContent="center"
      >
        <${Typography} variant="h6"> No comrades found 😢 <//>
      <//>
    `;
  }

  return html`
    <${Grid} container spacing=${2} alignItems="center" justifyContent="center">
      ${users.map(
        (user) => html`<${Grid} item><${UserCard} user=${user} /><//>`
      )}
    <//>
  `;
};
