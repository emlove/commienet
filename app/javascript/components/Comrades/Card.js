import html from "lib/htmCreateElement";

import { Link } from "react-router-dom";

import Avatar from "@mui/material/Avatar";
import Stack from "@mui/material/Stack";
import Card from "@mui/material/Card";
import CardActionArea from "@mui/material/CardActionArea";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Chip from "@mui/material/Chip";
import Typography from "@mui/material/Typography";

export default ({ user }) => {
  return html`
    <${Card}>
      <${CardActionArea} component=${Link} to=${`/profile/${user.id}`}>
        <${CardHeader}
          title=${user.name}
          avatar=${html`<${Avatar} src=${user.avatarUrl || undefined} />`}
        />
        <${CardContent}>
          <${Stack} direction="column" spacing=${1}>
            <${Typography}
              paragraph
              sx=${{
                whiteSpace: "pre-line",
                maxHeight: "4.5rem",
                overflow: "hidden",
              }}
            >
              ${user.bio}
            <//>
            <${Stack} direction="row" spacing=${1}>
              ${user.skills.map(
                (skill) => html`<${Chip} label=${skill.name} />`
              )}
            <//>
            <${Stack} direction="row" spacing=${1}>
              ${user.resources.map(
                (resource) => html`<${Chip} label=${resource.name} />`
              )}
            <//>
          <//>
        <//>
      <//>
    <//>
  `;
};
