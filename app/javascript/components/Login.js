import { useState } from "react";
import html from "lib/htmCreateElement";

import Alert from "@mui/material/Alert";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import CardHeader from "@mui/material/CardHeader";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";

export default ({ setAuthContext }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  const handleLogin = async () => {
    setIsSubmitting(true);
    setErrorMessage(null);
    const response = await fetch("/auth/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify({
        email,
        password,
      }),
    });
    setIsSubmitting(false);

    const result = await response.json();
    if (result.error) {
      setErrorMessage(result.error);
    } else {
      setAuthContext(result);
    }
  };

  const onFieldKeyPress = (e) => {
    if (e.which === 13) {
      e.preventDefault();
      return handleLogin();
    }
  };

  return html`
    <${Stack}
      spacing=${2}
      direction="column"
      alignItems="center"
      justifyContent="center"
      sx=${{ minHeight: "100vh" }}
    >
      <${Typography} variant="h3" color="primary.contrastText">Commienet<//>
      <${TextField}
        id="email"
        label="Email"
        required
        value=${email}
        onChange=${(e) => setEmail(e.target.value)}
        onKeyPress=${onFieldKeyPress}
      />
      <${TextField}
        id="password"
        label="Password"
        type="password"
        required
        value=${password}
        onChange=${(e) => setPassword(e.target.value)}
        onKeyPress=${onFieldKeyPress}
        error=${errorMessage}
        helperText=${errorMessage}
      />
      <${Button}
        id="submit"
        type="submit"
        variant="contained"
        onClick=${handleLogin}
        disabled=${isSubmitting}
      >
        Log In
      <//>
    <//>
  `;
};
