import html from "lib/htmCreateElement";
import { useContext } from "react";

import Avatar from "@mui/material/Avatar";
import Divider from "@mui/material/Divider";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import { Menu as MenuIcon } from "@mui/icons-material";
import { Link } from "react-router-dom";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";

import { Logout as LogoutIcon } from "@mui/icons-material";
import { AccountCircle as AccountCircleIcon } from "@mui/icons-material";

import { useCurrentUser } from "lib/sessionContext";
import { AuthContext } from "lib/api";

export default ({ open, onClose, children }) => {
  const currentUser = useCurrentUser();
  const authContext = useContext(AuthContext);

  return html`
    <${SwipeableDrawer} anchor="left" open=${open} onClose=${onClose}>
      <${List}>
        <${ListItem}>
          <${ListItemButton}
            component=${Link}
            to=${`/profile/${currentUser.id}`}
          >
            ${currentUser.avatarUrl
              ? html`
                  <${ListItemAvatar}>
                    <${Avatar} src=${currentUser.avatarUrl} />
                  <//>
                `
              : html`
                  <${ListItemIcon}>
                    <${AccountCircleIcon} />
                  <//>
                `}
            <${ListItemText}> Profile <//>
          <//>
        <//>
        <${Divider} />
      <//>
      ${children}
      <${List}>
        <${Divider} />
        <${ListItem}>
          <${ListItemButton} onClick=${authContext.logout}>
            <${ListItemIcon}>
              <${LogoutIcon} />
            <//>
            <${ListItemText}> Log Out <//>
          <//>
        <//>
      <//>
    <//>
  `;
};
