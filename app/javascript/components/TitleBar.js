import { useEffect } from "react";
import html from "lib/htmCreateElement";

import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import { Menu as MenuIcon } from "@mui/icons-material";

export default ({ title, onMenuButtonClicked }) => {
  useEffect(() => {
    document.title = `Commienet${title ? ` - ${title}` : ""}`;
  }, [title]);

  return html`
    <${Box} sx=${{ mb: 2 }}>
      <${AppBar} position="static">
        <${Toolbar}>
          <${IconButton}
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx=${{ mr: 2 }}
            onClick=${(e) => onMenuButtonClicked()}
          >
            <${MenuIcon} />
          <//>
          <${Typography} variant="h6" component="div" sx=${{ flexGrow: 1 }}>
            ${title}
          <//>
        <//>
      <//>
    <//>
  `;
};
