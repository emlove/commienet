import html from "lib/htmCreateElement";

import { Link } from "react-router-dom";

import Stack from "@mui/material/Stack";
import Card from "@mui/material/Card";
import CardActionArea from "@mui/material/CardActionArea";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Chip from "@mui/material/Chip";
import Typography from "@mui/material/Typography";

export default ({ dream }) => {
  return html`
    <${Card}>
      <${CardActionArea} component=${Link} to=${`/dream/${dream.id}`}>
        <${CardHeader} title=${dream.name} />
        <${CardContent}>
          <${Stack} direction="column" spacing=${1}>
            <${Typography} paragraph sx=${{ whiteSpace: "pre-line" }}>
              ${dream.description}
            <//>
            <${Stack} direction="row" spacing=${1}>
              ${dream.skills.map(
                (skill) => html`<${Chip} label=${skill.name} />`
              )}
            <//>
            <${Stack} direction="row" spacing=${1}>
              ${dream.resources.map(
                (resource) => html`<${Chip} label=${resource.name} />`
              )}
            <//>
          <//>
        <//>
      <//>
    <//>
  `;
};
