import html from "lib/htmCreateElement";
import { useState, useEffect } from "react";

import { Link } from "react-router-dom";

import Alert from "@mui/material/Alert";
import Fab from "@mui/material/Fab";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";

import { Add as AddIcon } from "@mui/icons-material";

import { useApi } from "lib/api";

import DreamCard from "components/Dreams/Card";
import Loading from "components/Shared/Loading";

export default ({ setPageTitle }) => {
  const { apiGet } = useApi();
  const [errorMessage, setErrorMessage] = useState(null);
  const [dreams, setDreams] = useState(null);

  useEffect(() => {
    setPageTitle("Dreams");
    setErrorMessage(null);

    apiGet("dreams")
      .then(setDreams)
      .catch((e) => setErrorMessage(e.message));
  }, []);

  if (errorMessage) {
    return html`<${Alert} severity="error">${errorMessage}<//>`;
  }

  if (dreams === null) {
    return html`<${Loading} />`;
  }

  const renderFab = () => html`<${Fab}
    color="primary"
    aria-label="add"
    sx=${{ position: "fixed", right: 8, bottom: 8 }}
    component=${Link}
    to="/dream/new"
  >
    <${AddIcon} />
  <//>`;

  if (dreams.length === 0) {
    return html`
      <${Stack}
        spacing=${2}
        direction="column"
        alignItems="center"
        justifyContent="center"
      >
        <${Typography} variant="h6"> No dreams found 😢 <//>
      <//>
      ${renderFab()}
    `;
  }

  return html`
    <${Grid} container spacing=${2} alignItems="center" justifyContent="center">
      ${dreams.map(
        (dream) => html` <${Grid} item><${DreamCard} dream=${dream} /><//> `
      )}
    <//>
    ${renderFab()}
  `;
};
