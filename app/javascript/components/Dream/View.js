import html from "lib/htmCreateElement";

import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import CardHeader from "@mui/material/CardHeader";
import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";

import SkillCard from "components/Dream/Skill/Card";
import ResourceCard from "components/Dream/Resource/Card";

export default ({ dream }) => {
  return html`
    <${Stack} spacing=${2} alignItems="center" justifyContent="center">
      <${CardHeader}
        title=${html`<${Typography} variant="h3">${dream.name}<//>`}
      />
      <${Typography} variant="body1">${dream.description}<//>
      <${Divider} />
      <${Grid}
        container
        spacing=${2}
        alignItems="center"
        justifyContent="center"
      >
        ${dream.skills.map(
          (skill) => html`
            <${Grid} item>
              <${SkillCard} skill=${skill} />
            <//>
          `
        )}
      <//>
      <${Divider} />
      <${Grid}
        container
        spacing=${2}
        alignItems="center"
        justifyContent="center"
      >
        ${dream.resources.map(
          (resource) => html`
            <${Grid} item>
              <${ResourceCard} resource=${resource} />
            <//>
          `
        )}
      <//>
    </${Stack}>
  `;
};
