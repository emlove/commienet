import { useState } from "react";
import html from "lib/htmCreateElement";

import Card from "@mui/material/Card";
import CardActionArea from "@mui/material/CardActionArea";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

import SkillAddForm from "components/Dream/Skill/AddForm";

export default ({ skillType, onClick }) => {
  return html`
    <${Card} elevation=${3}>
      <${CardActionArea} onClick=${onClick}>
        <${CardContent}>
          <${Typography} variant="h4"> ${skillType.name} <//>
        <//>
      <//>
    <//>
  `;
};
