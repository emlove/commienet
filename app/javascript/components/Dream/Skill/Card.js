import html from "lib/htmCreateElement";

import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

export default ({ skill }) => {
  return html`
    <${Card}>
      <${CardHeader} title=${skill.name} />
      <${CardContent}>
        <${Typography} paragraph sx=${{ whiteSpace: "pre-line" }}>
          ${skill.description}
        <//>
      <//>
    <//>
  `;
};
