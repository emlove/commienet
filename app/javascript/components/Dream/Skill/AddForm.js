import { useState, useContext } from "react";
import html from "lib/htmCreateElement";

import Grid from "@mui/material/Grid";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";

import SkillSelectCard from "components/Dream/Skill/SelectCard";
import { SessionContext } from "lib/sessionContext";

export default ({ onSkillAdded }) => {
  const [type, setType] = useState(null);
  const skillTypes = useContext(SessionContext).skillTypes;

  return html`
    <${Grid} container spacing=${2}>
      ${skillTypes.map(
        (skillType) => html` <${Grid} item>
          <${SkillSelectCard}
            skillType=${skillType}
            onClick=${() => onSkillAdded(skillType)}
          />
        <//>`
      )}
    <//>
  `;
};
