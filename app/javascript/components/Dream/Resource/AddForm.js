import { useState, useContext } from "react";
import html from "lib/htmCreateElement";

import Grid from "@mui/material/Grid";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";

import ResourceSelectCard from "components/Dream/Resource/SelectCard";
import { SessionContext } from "lib/sessionContext";

import { useApi } from "lib/api";

export default ({ onResourceAdded }) => {
  const { apiPost } = useApi();
  const [type, setType] = useState(null);
  const resourceTypes = useContext(SessionContext).resourceTypes;

  return html`
    <${Grid} container spacing=${2}>
      ${resourceTypes.map(
        (resourceType) => html` <${Grid} item>
          <${ResourceSelectCard}
            resourceType=${resourceType}
            onClick=${() => onResourceAdded(resourceType)}
          />
        <//>`
      )}
    <//>
  `;
};
