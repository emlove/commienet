import { useState } from "react";
import html from "lib/htmCreateElement";

import Card from "@mui/material/Card";
import CardActionArea from "@mui/material/CardActionArea";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

import ResourceAddForm from "components/Dream/Resource/AddForm";

export default ({ resourceType, onClick }) => {
  return html`
    <${Card} elevation=${3}>
      <${CardActionArea} onClick=${onClick}>
        <${CardContent}>
          <${Typography} variant="h4"> ${resourceType.name} <//>
        <//>
      <//>
    <//>
  `;
};
