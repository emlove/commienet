import { useState } from "react";
import html from "lib/htmCreateElement";

import Alert from "@mui/material/Alert";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";

import ResourceAddForm from "components/Dream/Resource/AddForm";

import { useApi } from "lib/api";

export default ({ dream, setDream }) => {
  const { apiPost } = useApi();
  const [dialogOpen, setDialogOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  const onResourceAdded = async (resourceType) => {
    setErrorMessage(null);
    try {
      setDream(
        await apiPost(`dream/${dream.id}/add_resource`, {
          resource_type: resourceType.type,
        })
      );
      setDialogOpen(false);
    } catch (e) {
      setErrorMessage(e.message);
    }
  };

  return html`
    <${Button} id="add" variant="outlined" onClick=${() => setDialogOpen(true)}>
      Add a resource
    <//>
    <${Dialog} open=${dialogOpen} onClose=${() => setDialogOpen(false)}>
      <${DialogTitle}>Add a resource<//>
      <${DialogContent}>
        ${errorMessage
          ? html`<${Alert} severity="error" sx=${{ marginBottom: 2 }}
              >${errorMessage}<//
            >`
          : null}
        <${ResourceAddForm} onResourceAdded=${onResourceAdded} />
      <//>
    <//>
  `;
};
