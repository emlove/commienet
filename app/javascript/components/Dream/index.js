import { useState, useEffect } from "react";
import { Navigate, useParams } from "react-router-dom";
import html from "lib/htmCreateElement";

import CircularProgress from "@mui/material/CircularProgress";

import { useApi } from "lib/api";
import { useCurrentUser } from "lib/sessionContext";

import DreamView from "components/Dream/View";
import DreamEdit from "components/Dream/Edit";

export default ({ setPageTitle }) => {
  const idString = useParams()["id"];
  const id = idString === "new" ? null : parseInt(idString);
  const { apiGet } = useApi();
  const [dream, setDream] = useState(id === null ? { id } : null);

  useEffect(() => {
    if (id === null) {
      setPageTitle("Share a dream");
    } else {
      apiGet(`dream/${id}`).then((dream) => {
        setDream(dream);
        setPageTitle(dream.name);
      });
    }
  }, [id]);

  if (dream === null) {
    return html`<${CircularProgress} />`;
  }

  if (id === null && dream?.id !== null) {
    return html`<${Navigate} to=${`/dream/${dream.id}`} />`;
  }

  if (id === null || useCurrentUser().id === dream.user.id) {
    return html`<${DreamEdit}
      key=${dream.id}
      dream=${dream}
      setDream=${setDream}
    />`;
  }

  return html`<${DreamView} key=${dream.id} dream=${dream} />`;
};
