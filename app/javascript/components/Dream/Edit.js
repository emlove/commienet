import { createRef, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import html from "lib/htmCreateElement";

import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import CardHeader from "@mui/material/CardHeader";
import TextField from "@mui/material/TextField";
import Divider from "@mui/material/Divider";
import Badge from "@mui/material/Badge";
import Stack from "@mui/material/Stack";

import { Delete as DeleteIcon } from "@mui/icons-material";

import SkillAddButton from "components/Dream/Skill/AddButton";
import SkillEditCard from "components/Dream/Skill/EditCard";
import ResourceAddButton from "components/Dream/Resource/AddButton";
import ResourceEditCard from "components/Dream/Resource/EditCard";

import SubmitButton from "components/Shared/SubmitButton";

import { useApi } from "lib/api";

export default ({ dream, setDream }) => {
  const navigate = useNavigate();
  const { apiPost, apiPut, apiDelete } = useApi();
  const [name, setName] = useState(dream?.name);
  const [description, setDescription] = useState(dream?.description);

  const handleSaveDream = async () => {
    const dreamData = {
      name,
      description,
    };

    if (dream.id === null) {
      setDream(await apiPut(`dream`, dreamData));
    } else {
      setDream(await apiPost(`dream/${dream.id}`, dreamData));
    }
  };

  const handleDeleteDream = async () => {
    await apiDelete(`dream/${dream.id}`);
    navigate("/dreams");
  };

  return html`
    <${Stack} spacing=${2} alignItems="center" justifyContent="center">
      <${TextField}
        id="name"
        label="What is your dream?"
        value=${name}
        onChange=${(e) => setName(e.target.value)}
        fullWidth
      />
      <${TextField}
        id="description"
        label="Details"
        multiline
        fullWidth
        value=${description}
        onChange=${(e) => setDescription(e.target.value)}
        minRows=${4}
      />
      <${Stack}
        direction="row"
        justifyContent="end"
        alignItems="center"
        sx=${{ width: "100%" }}
      >
        <${SubmitButton} onClick=${handleSaveDream} successText="Saved!"
          >Save<//
        >
      <//>
      ${dream.id
        ? html`
            <${Divider} />
            <${Grid}
              container
              spacing=${2}
              alignItems="center"
              justifyContent="center"
            >
              ${dream.skills.map(
                (skill) => html`<${Grid} item>
                  <${SkillEditCard}
                    dream=${dream}
                    setDream=${setDream}
                    skill=${skill}
                  />
                <//>`
              )}
              <${Grid} item>
                <${SkillAddButton} dream=${dream} setDream=${setDream} />
              <//>
            <//>
            <${Divider} />
            <${Grid}
              container
              spacing=${2}
              alignItems="center"
              justifyContent="center"
            >
              ${dream.resources.map(
                (resource) => html`<${Grid} item>
                  <${ResourceEditCard}
                    dream=${dream}
                    setDream=${setDream}
                    resource=${resource}
                  />
                <//>`
              )}
              <${Grid} item>
                <${ResourceAddButton} dream=${dream} setDream=${setDream} />
              <//>
            <//>
            <${Divider} />
            <${SubmitButton} onClick=${handleDeleteDream}>Delete<//>
          `
        : undefined}
    <//>
  `;
};
