import { render } from "react-dom";
import html from "lib/htmCreateElement";

import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "@mui/material/styles";

import Auth from "components/Auth";
import App from "components/App";

import theme from "lib/theme";

render(
  html`
    <${ThemeProvider} theme=${theme}>
      <${BrowserRouter}>
        <${Auth}>
          <${App} />
        <//>
      <//>
    </${ThemeProvider}>
  `,
  document.body
);
