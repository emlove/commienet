import { createContext, useContext } from "react";

export class ApiError extends Error {
  constructor(result) {
    super(result.error);
    this.result = result;
  }
}

export const AuthContext = createContext({});

export const useApi = () => {
  const authContext = useContext(AuthContext);
  const token = authContext.token;

  const apiFetch = async (url, method, body = null) => {
    const response = await fetch(`/api/${url}`, {
      method,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
      ...(body ? { body } : {}),
    });
    if (!response.ok) {
      if (response.status === 401) {
        authContext.logout();
      }

      const error = response.statusText || `HTTP ${response.status}`;
      throw new ApiError({ error });
    }

    const result = await response.json();
    if (result.error) {
      console.log(result.error, { result });
      throw new ApiError(result);
    }

    return result;
  };

  const apiGet = async (url, params = null) => {
    return apiFetch(
      params ? `${url}?${new URLSearchParams(params).toString()}` : url,
      "GET"
    );
  };

  const apiPost = async (url, params = null) => {
    return apiFetch(url, "POST", JSON.stringify(params));
  };

  const apiPostFiles = async (url, files) => {
    return new Promise((resolve, reject) => {
      const formData = new FormData();
      Object.entries(files).forEach(([fileName, file]) => {
        formData.append(fileName, file);
      });

      const xhr = new XMLHttpRequest();

      xhr.open("POST", `/api/${url}`);
      xhr.setRequestHeader("Accept", "application/json");
      xhr.setRequestHeader("Authorization", `Bearer ${token}`);
      xhr.responseType = "json";

      xhr.onload = function () {
        if (this.status >= 200 && this.status < 300) {
          resolve(xhr.response);
        } else {
          reject({
            error: this.status || xhr.statusText,
          });
        }
      };
      xhr.onerror = function () {
        reject({
          error: this.status || xhr.statusText,
        });
      };
      xhr.send(formData);
    });
  };

  const apiPut = async (url, params = null) => {
    return apiFetch(url, "PUT", JSON.stringify(params));
  };

  const apiDelete = async (url, params = null) => {
    return apiFetch(url, "DELETE", JSON.stringify(params));
  };

  return {
    apiGet,
    apiPost,
    apiPostFiles,
    apiPut,
    apiDelete,
  };
};
