import { createTheme } from "@mui/material/styles";

export default createTheme({
  palette: {
    primary: {
      main: "#ff6cc3",
      contrastText: "#4e1b70",
    },
    secondary: {
      main: "#6aaded",
      contrastText: "#4e1b70",
    },
  },
});
