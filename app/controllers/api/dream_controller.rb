class Api::DreamController < ApplicationController
  def get_all
    render json: Dream.all.map { |dream| serialize_dream(dream) }
  end

  def get
    dream = Dream.find(params.require(:id))
    render json: serialize_dream(dream)
  end

  def put
    dream =
      Dream.create(
        user_id: current_user.id,
        name: params.require(:name),
        description: params.require(:description)
      )

    render json: serialize_dream(dream)
  end

  def delete
    editable_dream.destroy
    render json: serialize_dream(editable_dream)
  end

  def post
    editable_dream.name = params[:name] if params.key?(:name)
    editable_dream.description = params[:description] if params.key?(
      :description
    )
    editable_dream.save!

    render json: serialize_dream(editable_dream)
  end

  def add_skill
    skill = editable_dream.skills.new({ type: params.require(:skill_type) })
    skill.description = params[:description] if params.key?(:description)
    skill.save!
    render json: serialize_dream(editable_dream)
  end

  def edit_skill
    skill = editable_dream.skills.find(params.require(:skill_id))
    skill.description = params[:description] if params.key?(:description)
    skill.save!
    render json: serialize_dream(editable_dream)
  end

  def delete_skill
    editable_dream.skills.destroy(params.require(:skill_id))
    render json: serialize_dream(editable_dream)
  end

  def add_resource
    resource =
      editable_dream.resources.new({ type: params.require(:resource_type) })
    resource.description = params[:description] if params.key?(:description)
    resource.save!
    render json: serialize_dream(editable_dream)
  end

  def edit_resource
    resource = editable_dream.resources.find(params.require(:resource_id))
    resource.description = params[:description] if params.key?(:description)
    resource.save!
    render json: serialize_dream(editable_dream)
  end

  def delete_resource
    editable_dream.resources.destroy(params.require(:resource_id))
    render json: serialize_dream(editable_dream)
  end

  private

  def editable_dream
    @editable_dream ||=
      Dream
        .find(params.require(:id))
        .tap do |editable_dream|
          if editable_dream.user_id != current_user.id
            raise ForbiddenError.new('Cannot edit other users’ dreams')
          end
        end
  end

  def serialize_dream(dream)
    {
      id: dream.id,
      name: dream.name,
      description: dream.description,
      skills: dream.skills.map { |skill| serialize_dream_skill(skill) },
      resources:
        dream.resources.map { |resource| serialize_dream_resource(resource) },
      user: serialize_user(dream.user)
    }
  end

  def serialize_dream_skill(skill)
    {
      id: skill.id,
      type: skill.type,
      name: skill.name,
      description: skill.description
    }
  end

  def serialize_dream_resource(resource)
    {
      id: resource.id,
      type: resource.type,
      name: resource.name,
      description: resource.description
    }
  end

  def serialize_user(user)
    { id: user.id }
  end
end
