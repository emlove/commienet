class Api::UserController < ApplicationController
  def get_all
    render json: User.all.map { |user| serialize_user(user) }
  end

  def get
    user = User.find(params.require(:id))

    render json: serialize_user(user)
  end

  def post
    editable_user.name = params[:name] if params.key?(:name)
    editable_user.bio = params[:bio] if params.key?(:bio)
    editable_user.save!

    render json: serialize_user(editable_user)
  end

  def upload_avatar
    editable_user.avatar.purge if editable_user.avatar.attached?
    editable_user.avatar.attach(params.require(:avatar))

    render json: serialize_user(editable_user)
  end

  def delete_avatar
    editable_user.avatar.purge if editable_user.avatar.attached?

    render json: serialize_user(editable_user)
  end

  def add_skill
    skill = editable_user.skills.new({ type: params.require(:skill_type) })
    skill.description = params[:description] if params.key?(:description)
    skill.save!
    render json: serialize_user(editable_user)
  end

  def edit_skill
    skill = editable_user.skills.find(params.require(:skill_id))
    skill.description = params[:description] if params.key?(:description)
    skill.save!
    render json: serialize_user(editable_user)
  end

  def delete_skill
    editable_user.skills.destroy(params.require(:skill_id))
    render json: serialize_user(editable_user)
  end

  def add_resource
    resource =
      editable_user.resources.new({ type: params.require(:resource_type) })
    resource.description = params[:description] if params.key?(:description)
    resource.save!
    render json: serialize_user(editable_user)
  end

  def edit_resource
    resource = editable_user.resources.find(params.require(:resource_id))
    resource.description = params[:description] if params.key?(:description)
    resource.save!
    render json: serialize_user(editable_user)
  end

  def delete_resource
    editable_user.resources.destroy(params.require(:resource_id))
    render json: serialize_user(editable_user)
  end

  private

  def editable_user
    @editable_user ||=
      User
        .find(params.require(:id))
        .tap do |editable_user|
          if editable_user.id != current_user.id
            raise ForbiddenError.new('Cannot edit other users')
          end
        end
  end

  def serialize_user(user)
    {
      id: user.id,
      name: user.name,
      avatarUrl: user.avatar.attached? ? url_for(user.avatar) : nil,
      bio: user.bio,
      email: user.email,
      skills: user.skills.map { |skill| serialize_user_skill(skill) },
      resources:
        user.resources.map { |resource| serialize_user_resource(resource) }
    }
  end

  def serialize_user_skill(skill)
    {
      id: skill.id,
      type: skill.type,
      name: skill.name,
      description: skill.description
    }
  end

  def serialize_user_resource(resource)
    {
      id: resource.id,
      type: resource.type,
      name: resource.name,
      description: resource.description
    }
  end
end
