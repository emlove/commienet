class Api::SessionContextController < ApplicationController
  def get
    render json: {
             currentUser: {
               id: current_user.id,
               name: current_user.name,
               email: current_user.email,
               avatarUrl:
                 if current_user.avatar.attached?
                   url_for(current_user.avatar)
                 else
                   nil
                 end
             },
             skillTypes:
               skill_types.map do |skill_type|
                 { name: Skills::TYPE_NAMES[skill_type], type: skill_type }
               end,
             resourceTypes:
               resource_types.map do |resource_type|
                 {
                   name: Resources::TYPE_NAMES[resource_type],
                   type: resource_type
                 }
               end
           }
  end

  private

  def skill_types
    Skills::TYPE_ENUM.keys
  end

  def resource_types
    Resources::TYPE_ENUM.keys
  end
end
