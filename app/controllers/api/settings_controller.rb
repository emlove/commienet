class Api::SettingsController < ApplicationController
  def get
    render_settings
  end

  def post
    current_user.email = params.require(:email)
    current_user.password = params[:password] if params.key?(:password)
    current_user.save!

    render_settings
  end

  private

  def render_settings
    render json: { email: current_user.email }
  end
end
