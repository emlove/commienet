require 'jwt'

module JsonWebToken
  extend ActiveSupport::Concern
  JWT_SECRET = ENV['JWT_SECRET']

  def jwt_encode(payload, exp = 7.days.from_now)
    payload[:exp] = exp.to_i
    JWT.encode(payload, JWT_SECRET)
  end

  def jwt_decode(token)
    JWT.decode(token, JWT_SECRET)[0].with_indifferent_access
  end
end
