class AuthenticationController < ApplicationController
  skip_before_action :authenticate_request

  def login
    args = JSON.parse(request.body.read)
    user = User.find_by_email(args['email'])
    if user&.authenticate(args['password'])
      token = jwt_encode(user_id: user.id)
      render json: { token: token }
    else
      render json: {
               error: 'Email or password incorrect'
             },
             status: :unauthorized
    end
  end
end
