class ApplicationController < ActionController::Base
  class ForbiddenError < RuntimeError
  end

  attr_reader :current_user

  rescue_from JWT::ExpiredSignature, with: :render_unauthorized_error
  rescue_from ForbiddenError, with: :render_forbidden_error
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found

  include JsonWebToken

  before_action :authenticate_request

  protect_from_forgery with: :null_session

  private

  def authenticate_request
    header = request.headers['Authorization']&.split(' ')&.last
    decoded = jwt_decode(header)
    @current_user = User.find(decoded[:user_id])
  end

  def render_unauthorized_error(exception)
    render json: { error: exception.message }, status: :unauthorized
  end

  def render_forbidden_error(exception)
    render json: { error: exception.message }, status: :forbidden
  end

  def render_not_found(exception)
    render json: { error: exception.message }, status: :not_found
  end
end
