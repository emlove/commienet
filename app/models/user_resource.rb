# == Schema Information
#
# Table name: user_resources
#
#  id          :integer          not null, primary key
#  description :string           default(""), not null
#  type        :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer          not null
#
# Indexes
#
#  index_user_resources_on_user_id_and_type  (user_id,type) UNIQUE
#
class UserResource < ApplicationRecord
  self.inheritance_column = :_type_disabled

  belongs_to :user

  enum type: Resources::TYPE_ENUM

  def name
    Resources::TYPE_NAMES[type]
  end
end
