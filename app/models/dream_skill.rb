# == Schema Information
#
# Table name: dream_skills
#
#  id          :integer          not null, primary key
#  description :string
#  type        :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  dream_id    :integer          not null
#
# Indexes
#
#  index_dream_skills_on_dream_id  (dream_id)
#
# Foreign Keys
#
#  dream_id  (dream_id => dreams.id)
#
class DreamSkill < ApplicationRecord
  self.inheritance_column = :_type_disabled

  belongs_to :dream

  enum type: Skills::TYPE_ENUM

  def name
    Skills::TYPE_NAMES[type]
  end
end
