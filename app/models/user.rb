# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  bio             :string           default(""), not null
#  email           :string           not null
#  name            :string           not null
#  password_digest :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#
class User < ApplicationRecord
  has_many :skills, class_name: :UserSkill, dependent: :destroy
  has_many :resources, class_name: :UserResource, dependent: :destroy
  has_many :dreams, class_name: :Dream, dependent: :destroy

  has_secure_password

  has_one_attached :avatar
end
