# == Schema Information
#
# Table name: dream_resources
#
#  id          :integer          not null, primary key
#  description :string
#  type        :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  dream_id    :integer          not null
#
# Indexes
#
#  index_dream_resources_on_dream_id  (dream_id)
#
# Foreign Keys
#
#  dream_id  (dream_id => dreams.id)
#
class DreamResource < ApplicationRecord
  self.inheritance_column = :_type_disabled

  belongs_to :dream

  enum type: Resources::TYPE_ENUM

  def name
    Resources::TYPE_NAMES[type]
  end
end
