# == Schema Information
#
# Table name: dreams
#
#  id          :integer          not null, primary key
#  description :string           default(""), not null
#  name        :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer          not null
#
# Indexes
#
#  index_dreams_on_user_id  (user_id)
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Dream < ApplicationRecord
  belongs_to :user

  has_many :skills, class_name: :DreamSkill, dependent: :destroy
  has_many :resources, class_name: :DreamResource, dependent: :destroy
end
