# == Schema Information
#
# Table name: user_skills
#
#  id          :integer          not null, primary key
#  description :string           default(""), not null
#  type        :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer          not null
#
# Indexes
#
#  index_user_skills_on_user_id_and_type  (user_id,type) UNIQUE
#
class UserSkill < ApplicationRecord
  self.inheritance_column = :_type_disabled

  belongs_to :user

  enum type: Skills::TYPE_ENUM

  def name
    Skills::TYPE_NAMES[type]
  end
end
